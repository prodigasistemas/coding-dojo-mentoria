package triangulo;

public class Triangulo {

	public Boolean isTrianguloValido(int a, int b, int c) {
		return (a < b + c) && (b < a + c) && (c < a + b) ;
	}

	public Boolean isTrianguloEquilatero(int a, int b, int c) {
		return (a == b) && (b == c);
	}

	public Boolean isTrianguloIsosceles(int a, int b, int c) {
		return (((a == b) && (a != c))
				||((a == c) && (a != b))
				||((b == c) && (c != a)));
	}
	
	public Boolean isTrianguloEscaleno(int a, int b, int c) {
		return ((a != b) && (b != c) && (a != c));
	}
	
	public String getTipoTriangulo(int a, int b, int c) {
		
		if (isTrianguloValido(a, b, c)) {
			
			if (isTrianguloEquilatero(a, b, c)) {
				return "Tri�ngulo Equil�tero";
				
			} else if (isTrianguloIsosceles(a, b, c)) {
				return "Tri�ngulo Is�sceles";
				
			} else if (isTrianguloEscaleno(a, b, c)) {
				return "Tri�ngulo Escaleno";
			} else {
				return "Outro tipo de tri�ngulo";
			}
		} else {
			return "N�o � um tri�ngulo";
		}
	}
}


