package triangulo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ClassificadorTrianguloTest {

	@Test
	void testTrianguloInvalido() {
		Triangulo t = new Triangulo();

		assertFalse(t.isTrianguloValido(1, 2, 3));
	}
	
	@Test
	void testTrianguloValido() {
		Triangulo t = new Triangulo();

		assertTrue(t.isTrianguloValido(1, 1, 1));
	}
	
	@Test
	void testTrianguloEquilatero() {
		Triangulo t = new Triangulo();

		assertTrue(t.isTrianguloEquilatero(1, 1, 1));
	}
	
	@Test
	void testTrianguloEquilateroInvalido() {
		Triangulo t = new Triangulo();

		assertFalse(t.isTrianguloEquilatero(1, 1, 2));
	}
	
	@Test
	void testTrianguloIsosceles() {
		Triangulo t = new Triangulo();

		assertTrue(t.isTrianguloIsosceles(1, 1, 2));
	}
	
	@Test
	void testTrianguloIsoscelesInvalido() {
		Triangulo t = new Triangulo();

		assertFalse(t.isTrianguloIsosceles(1, 2, 3));
	}
	
	@Test
	void testTrianguloEscaleno() {
		Triangulo t = new Triangulo();

		assertTrue(t.isTrianguloEscaleno(1, 2, 3));
	}
	
	@Test
	void testTrianguloEscalenoInvalido() {
		Triangulo t = new Triangulo();

		assertFalse(t.isTrianguloEscaleno(1, 1, 3));
	}
	
	@Test
	void testTipoTrianguloEquilatero() {
		Triangulo t = new Triangulo();

		assertEquals("Tri�ngulo Equil�tero", t.getTipoTriangulo(1, 1, 1));
	}
	
	@Test
	void testTipoTrianguloIsosceles() {
		Triangulo t = new Triangulo();

		assertEquals("Tri�ngulo Is�sceles", t.getTipoTriangulo(1, 1, 3));
	}
	
	@Test
	void testTipoTrianguloEscaleno() {
		Triangulo t = new Triangulo();

		assertEquals("Tri�ngulo Escaleno", t.getTipoTriangulo(2, 3, 5));
	}
	
	@Test
	void testTipoTrianguloFalso() {
		Triangulo t = new Triangulo();

		assertEquals("N�o � um tri�ngulo", t.getTipoTriangulo(1, 1, 2));
	}
}
